﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.TagViewModels;

        public class TagProfile : Profile
    {
        public TagProfile()
        {
            CreateMap<TagViewModel, Tag>();
            CreateMap<Tag, TagViewModel>();
        }
    }
}