﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.RecipeTagViewModels;

    public class RecipeTagProfile : Profile
    {
        public RecipeTagProfile()
        {
            CreateMap<RecipeTagViewModel, RecipeTag>();
            CreateMap<RecipeTag, RecipeTagViewModel>();
        }
    }
}