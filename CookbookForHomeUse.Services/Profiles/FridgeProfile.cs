﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.FridgeViewModels;

    public  class FridgeProfile : Profile
    {
        public FridgeProfile()
        {
            CreateMap<FridgeViewModel, Fridge>();
            CreateMap<Fridge, FridgeViewModel>();
        }
    }
}
