﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.ProductViewModels;

    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<ProductViewModel, Product>();
            CreateMap<Product, ProductViewModel>();
        }
    }
}