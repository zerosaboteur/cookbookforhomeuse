﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.RecipeIngredientViewModels;

        public class RecipeIngredientProfile : Profile
    {
        public RecipeIngredientProfile()
        {
            CreateMap<RecipeIngredientViewModel, RecipeIngredient>();
            CreateMap<RecipeIngredient, RecipeIngredientViewModel>();
        }
    }
}