﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.RecipeViewModels;

    public class RecipeProfile : Profile
    {
        public RecipeProfile()
        {
            CreateMap<RecipeViewModel, Recipe>();
            CreateMap<Recipe, RecipeViewModel>();
        }
    }
}