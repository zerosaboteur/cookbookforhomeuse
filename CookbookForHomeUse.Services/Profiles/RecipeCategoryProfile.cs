﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.RecipeCategoryViewModels;

    public class RecipeCategoryProfile : Profile
    {
        public RecipeCategoryProfile()
        {
            CreateMap<RecipeCategoryViewModel, RecipeCategory>();
            CreateMap<RecipeCategory, RecipeCategoryViewModel>();
        }
    }
}