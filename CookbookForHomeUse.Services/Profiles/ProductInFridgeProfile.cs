﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.ProductInFridgeViewModels;

    public class ProductInFridgeProfile : Profile
    {
        public ProductInFridgeProfile()
        {
            CreateMap<ProductInFridgeViewModel, ProductInFridge>();
            CreateMap<ProductInFridge, ProductInFridgeViewModel>();
        }
    }
}