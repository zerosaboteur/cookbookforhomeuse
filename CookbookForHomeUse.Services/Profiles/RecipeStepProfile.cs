﻿namespace CookbookForHomeUse.Services.Profiles
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.ViewModels.RecipeStepViewModels;

        public class RecipeStepProfile : Profile
    {
        public RecipeStepProfile()
        {
            CreateMap<RecipeStepViewModel, RecipeStep>();
            CreateMap<RecipeStep, RecipeStepViewModel>();
        }
    }
}