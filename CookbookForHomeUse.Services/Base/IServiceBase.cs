﻿namespace CookbookForHomeUse.Services.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IServiceBase<TViewModel, TEntity> : IDisposable
    {
        /// <summary>
        /// Podstawowa metoda pobierajaca wszystkie dane z danej tabeli
        /// </summary>
        /// <returns> Obiekt z wartościami danej tabeli </returns>
        IEnumerable<TViewModel> Get();

        /// <summary>
        /// Metoda pobiera element z podanej tabeli ze wskzanym id
        /// </summary>
        /// <param name="id">Id suzkanego obiektu</param>
        /// <returns>Obiekt z wartościami danej tabeli</returns>
        Task<TViewModel> Get(Guid id);

        /// <summary>
        /// Metoda pobierająca dane ze wskazanej tabeli, które spelniają podane warunki
        /// </summary>
        /// <param name="expression">Warunki do spełnienia</param>
        /// <returns>Obiekt z wartościami danej tabeli</returns>
        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// Metoda sprawdza czy istnieje jakolwiek wiersz posaidajacy wsakzane id
        /// </summary>
        /// <param name="id">Id do sprawdzenia czy istnieje wiersz</param>
        /// <returns>Tak/Nie w zależności czy znaleziono wiersz</returns>
        Task<bool> Any(Guid id);

        /// <summary>
        /// Metoda sprawdza czy isytnieje jakikolwiek wiersz pasujacy do podanego wyrażenia
        /// </summary>
        /// <param name="expression">Wyreżenie które musi spełnić jakikolwiek wiersz</param>
        /// <returns>Tak/Nie w zależności czy wiersz istnieje</returns>
        Task<bool> Any(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// Metoda dodaje podany obiekt do wskazanej tabeli
        /// </summary>
        /// <param name="viewModel">Obiekt który odzwierciedla wygląd tabeli</param>
        /// <returns>Wynik dodania nowego wiersza</returns>
        Task Create(TViewModel viewModel);

        /// <summary>
        /// Metoda aktualizuje podany obiekt do wskazanej tabeli
        /// </summary>
        /// <param name="id">Id wiersza do zaktualizowania</param>
        /// <param name="viewModel">Obiekt który odzwierciedla wygląd tabeli</param>
        /// <returns>Wynik aktualizacji wiersza</returns>
        Task Update(Guid id, TViewModel viewModel);

        /// <summary>
        /// Metoda usuwa podany wiersz z tabeli
        /// </summary>
        /// <param name="id">Id wiersza do usunięcia</param>
        /// <returns>Wynik działania</returns>
        Task Delete(Guid id);

        /// <summary>
        /// Metoda zapisuje wprwoadzone zmiany
        /// </summary>
        /// <returns>Wynik zadania</returns>
        Task SaveAsync();
    }
}
