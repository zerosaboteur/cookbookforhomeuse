﻿namespace CookbookForHomeUse.Services.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    using Microsoft.EntityFrameworkCore;

    public class ServiceBase<TViewModel, TEntity> : IServiceBase<TViewModel, TEntity> where TEntity : BaseEntity
    {
        public ServiceBase(IMapper mapper, IRepository<TEntity> repository)
        {
            this.Mapper = mapper;
            this.Repository = repository;
        }

        protected IMapper Mapper { get; private set; }

        protected IRepository<TEntity> Repository { get; private set; }

        public virtual IEnumerable<TViewModel> Get()
        {
            var models = this.Repository.Get();
            return this.Mapper.Map<IEnumerable<TViewModel>>(models);
        }

        public virtual async Task<TViewModel> Get(Guid id)
        {
            var result = await this.Repository.Get(id);
            return this.Mapper.Map<TViewModel>(result);
        }

        public virtual async Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> expression)
        {
            var result = await this.Repository.Get(expression).ToListAsync();
            return result;
        }

        public virtual async Task<bool> Any(Guid id)
        {
            return await this.Repository.Any(id);
        }

        public virtual async Task<bool> Any(Expression<Func<TEntity, bool>> expression)
        {
            return await this.Repository.Any(expression);
        }

        public virtual async Task Create(TViewModel viewModel)
        {
            var model = this.Mapper.Map<TEntity>(viewModel);
            await this.Repository.Create(model);
        }

        public virtual async Task Update(Guid id, TViewModel viewModel)
        {
            var model = this.Mapper.Map<TEntity>(viewModel);
            model.Id = id;
            await this.Repository.Update(model);
        }

        public virtual async Task Delete(Guid id)
        {
            var model = await this.Repository.Get(id);
            this.Repository.Delete(model);
        }

        public virtual async Task SaveAsync()
        {
            await this.Repository.Save();
        }

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
