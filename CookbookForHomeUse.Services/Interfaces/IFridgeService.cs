﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.FridgeViewModels;

    public interface IFridgeService : IServiceBase<FridgeViewModel, Fridge>
    {
    }
}