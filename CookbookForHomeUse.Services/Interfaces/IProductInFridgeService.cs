﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.ProductInFridgeViewModels;

    public interface IProductInFridgeService : IServiceBase<ProductInFridgeViewModel, ProductInFridge>
    {
    }
}