﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.RecipeViewModels;

    public interface IRecipeService : IServiceBase<RecipeViewModel, Recipe>
    {
    }
}