﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.RecipeIngredientViewModels;

    public interface IRecipeIngredientService : IServiceBase<RecipeIngredientViewModel, RecipeIngredient>
    {
    }
}