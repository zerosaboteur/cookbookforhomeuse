﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.ProductViewModels;

    public interface IProductService : IServiceBase<ProductViewModel, Product>
    {
    }
}