﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.RecipeCategoryViewModels;

    public interface IRecipeCategoryService : IServiceBase<RecipeCategoryViewModel, RecipeCategory>
    {
    }
}