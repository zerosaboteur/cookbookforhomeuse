﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.RecipeStepViewModels;

    public interface IRecipeStepService : IServiceBase<RecipeStepViewModel, RecipeStep>
    {
    }
}