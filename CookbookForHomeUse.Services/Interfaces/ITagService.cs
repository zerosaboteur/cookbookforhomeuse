﻿namespace CookbookForHomeUse.Services.Interfaces
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.ViewModels.TagViewModels;

    public interface ITagService : IServiceBase<TagViewModel, Tag>
    {
    }
}