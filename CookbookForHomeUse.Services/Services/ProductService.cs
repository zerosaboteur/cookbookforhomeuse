﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.ProductViewModels;

    public class ProductService : ServiceBase<ProductViewModel, Product>, IProductService
    {
        public ProductService(IMapper mapper, IRepository<Product> repository)
            : base(mapper, repository)
        {
        }
    }
}