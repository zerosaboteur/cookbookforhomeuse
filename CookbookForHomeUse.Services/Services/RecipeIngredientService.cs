﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.RecipeIngredientViewModels;

    public class RecipeIngredientService : ServiceBase<RecipeIngredientViewModel, RecipeIngredient>, IRecipeIngredientService
    {
        public RecipeIngredientService(IMapper mapper, IRepository<RecipeIngredient> repository)
            : base(mapper, repository)
        {
        }
    }
}