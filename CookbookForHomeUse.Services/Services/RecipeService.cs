﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.RecipeViewModels;

    public class RecipeService : ServiceBase<RecipeViewModel, Recipe>, IRecipeService
    {
        public RecipeService(IMapper mapper, IRepository<Recipe> repository)
            : base(mapper, repository)
        {
        }
    }
}