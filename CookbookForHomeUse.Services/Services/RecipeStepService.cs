﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.RecipeStepViewModels;

    public class RecipeStepService : ServiceBase<RecipeStepViewModel, RecipeStep>, IRecipeStepService
    {
        public RecipeStepService(IMapper mapper, IRepository<RecipeStep> repository)
            : base(mapper, repository)
        {
        }
    }
}