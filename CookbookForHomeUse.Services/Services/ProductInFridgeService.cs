﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.ProductInFridgeViewModels;

    public class ProductInFridgeService : ServiceBase<ProductInFridgeViewModel, ProductInFridge>, IProductInFridgeService
    {
        public ProductInFridgeService(IMapper mapper, IRepository<ProductInFridge> repository)
            : base(mapper, repository)
        {
        }
    }
}