﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.RecipeTagViewModels;

    public class RecipeTagService : ServiceBase<RecipeTagViewModel, RecipeTag>, IRecipeTagService
    {
        public RecipeTagService(IMapper mapper, IRepository<RecipeTag> repository)
            : base(mapper, repository)
        {
        }
    }
}