﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.RecipeCategoryViewModels;

    public class RecipeCategoryService : ServiceBase<RecipeCategoryViewModel, RecipeCategory>, IRecipeCategoryService
    {
        public RecipeCategoryService(IMapper mapper, IRepository<RecipeCategory> repository)
            : base(mapper, repository)
        {
        }
    }
}