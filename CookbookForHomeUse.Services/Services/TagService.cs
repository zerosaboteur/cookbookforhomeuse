﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.TagViewModels;

    public class TagService : ServiceBase<TagViewModel, Tag>, ITagService
    {
        public TagService(IMapper mapper, IRepository<Tag> repository)
            : base(mapper, repository)
        {
        }
    }
}