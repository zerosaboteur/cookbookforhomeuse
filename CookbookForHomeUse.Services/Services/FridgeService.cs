﻿namespace CookbookForHomeUse.Services.Services
{
    using AutoMapper;

    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;
    using CookbookForHomeUse.Services.Base;
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.ViewModels.FridgeViewModels;

    public class FridgeService : ServiceBase<FridgeViewModel, Fridge>, IFridgeService
    {
        public FridgeService(IMapper mapper, IRepository<Fridge> repository)
            : base(mapper, repository)
        {
        }
    }
}