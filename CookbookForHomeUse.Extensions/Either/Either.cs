﻿using System;

namespace CookbookForHomeUse.Extensions.Either
{
    public class Either<TL, TR>
    {
        /// <summary>
        /// Kontruktor dla lewej zmiennej
        /// </summary>
        /// <param name="left">Wartość lewej zmiennej</param>
        public Either(TL left)
        {
            this.Left = left;
            this.IsLeft = true;
        }

        /// <summary>
        /// Konstruktor dla prawej zmiennej
        /// </summary>
        /// <param name="right">Wartość prawej zmiennej</param>
        public Either(TR right)
        {
            this.Right = right;
            this.IsLeft = false;
        }

        /// <summary>
        /// Lewa zmienna
        /// </summary>
        public TL Left { get; }

        /// <summary>
        /// Prawa zmienna
        /// </summary>
        public TR Right { get; }

        /// <summary>
        /// Zmienna informujaca czy wykorzystana zostąła zmienna lewa czy prawa
        /// </summary>
        public bool IsLeft { get; }

        /// <summary>
        /// Tworzy obiekt typu Either wykorzystując zmienną lewą
        /// </summary>
        /// <param name="left">Wartość lewej zmiennej</param>
        public static implicit operator Either<TL, TR>(TL left) => new Either<TL, TR>(left);

        /// <summary>
        /// Tworzy obiekt typu Either wykorzystując zmienną prawą
        /// </summary>
        /// <param name="right">Wartosć prawej zmiennej</param>
        public static implicit operator Either<TL, TR>(TR right) => new Either<TL, TR>(right);

        /// <summary>
        /// Bazowa metoda dopasowania wzorów.
        /// Jeśli podana jest wartość lewa, to Match zwróci wynik lewej funkcji, w przeciwnym razie wynik prawej funkcji.
        /// </summary>
        /// <typeparam name="T">Typ zwracanej wartości</typeparam>
        /// <param name="leftFunc">Lewa funkcja </param>
        /// <param name="rightFunc">Prawa funkcja</param>
        /// <returns>Wynik prawej/lewej funkcji</returns>
        public T Match<T>(Func<TL, T> leftFunc, Func<TR, T> rightFunc)
        {
            if (leftFunc == null)
            {
                throw new ArgumentNullException(nameof(leftFunc));
            }

            if (rightFunc == null)
            {
                throw new ArgumentNullException(nameof(rightFunc));
            }

            return this.IsLeft ? leftFunc(this.Left) : rightFunc(this.Right);
        }

        /// <summary>
        /// Bazowa metoda dopasowania wzorów.
        /// Jeśli podana jest wartość lewa, to Match wykona lewą funkcję, w przeciwnym razie wykona funkcję prawą.
        /// </summary>
        /// <param name="leftFunc">Lewa funkcja </param>
        /// <param name="rightFunc">Prawa funkcja</param>
        public void Match(Action<TL> leftFunc, Action<TR> rightFunc)
        {
            if (leftFunc == null)
            {
                throw new ArgumentNullException(nameof(leftFunc));
            }

            if (rightFunc == null)
            {
                throw new ArgumentNullException(nameof(rightFunc));
            }

            if (this.IsLeft)
            {
                leftFunc(this.Left);
            }
            else
            {
                rightFunc(this.Right);
            }
        }

        /// <summary>
        /// Funkcja ustala czy uzyta została lewa zmienna, jesli tak to zwraca jej wartość inaczej zwraca defaultowy obiekt
        /// o typie takim jak lewa zmienna
        /// </summary>
        /// <returns> Wartosć lewej zmiennej lub defaultową wartość o typie lewej zmiennej</returns>
        public TL LeftOrDefault() => this.Match(l => l, r => default(TL));

        /// <summary>
        /// Funkcja ustala czy uzyta została prawa zmienna, jesli tak to zwraca jej wartość inaczej zwraca defaultowy obiekt
        /// o typie takim jak lewa zmienna
        /// </summary>
        /// <returns> Wartosć lewej zmiennej lub defaultową wartość o typie prawej zmiennej</returns>
        public TR RightOrDefault() => this.Match(l => default(TR), r => r);
    }
}
