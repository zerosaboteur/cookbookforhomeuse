﻿using CookbookForHomeUse.Controllers;
using NUnit.Framework;

namespace CookbookForHomeUse.UnitTests.Controllers
{
    using CookbookForHomeUse.Extensions.Either;
    using CookbookForHomeUse.ViewModels.ApiResponseViewModels;

    using FluentAssertions;

    public class DummyTests
    {
        private DummyController DummyController { get; set; }

        [SetUp]
        public void Setup()
        {
            this.DummyController = new DummyController();
        }

        [Test]
        public void Test_ReturnError()
        {
            Either<SuccessResponse, ErrorResponse> expected = new Either<SuccessResponse, ErrorResponse>(new ErrorResponse(403, "User name not found or incorrect password"));

            //Either<SuccessResponse, ErrorResponse> response = this.DummyController.Test();

            //response.Should().BeEquivalentTo(expected);
        }
    }
}
