namespace CookbookForHomeUse
{
    using System;
    using System.IO;

    using CookbookForHomeUse.Configurations;
    using CookbookForHomeUse.Database;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.SpaServices.AngularCli;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;

    /// <summary>
    /// Klasa startowa projektu
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Konstruktor klasy startowej projektu
        /// </summary>
        /// <param name="configuration"> Klasa konfiguracji aplikacji, przechowuj�ca warto�ci ustawie� aplikacji </param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        /// <summary>
        /// Klasa konfiguracji aplikacji, przechowuj�ca warto�ci ustawie� aplikacji
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Metoda uruchamiana przez Runtime, w kt�rej dodawane s� serwisy do kontenera
        /// </summary>
        /// <param name="services"> Klasa zawieraj�ca kontrakty dla kolekcji serwis�w</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRepositories();
            services.AddServices();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(this.Configuration.GetConnectionString("CookBookContext")));

            services.AddSwaggerGen(
                s =>
                    {
                        s.SwaggerDoc("v1", new OpenApiInfo { Title = "Dost�pne API", Version = "v1" });
                        var basePath = AppContext.BaseDirectory;
                        var xmlPath = Path.Combine(basePath, "CookbookForHomeUse.xml");
                        s.IncludeXmlComments(xmlPath);
                        xmlPath = Path.Combine(basePath, "CookbookForHomeUse.ViewModels.xml");
                        s.IncludeXmlComments(xmlPath);
                    });

            // Na produkcji pliki angulara b�d� dost�pne pod t� �cie�k�
            services.AddSpaStaticFiles(configuration =>
                {
                    configuration.RootPath = "ClientApp/dist";
                });
        }

        /// <summary>
        /// Metoda uruchamiana przez Runtime, s�u�y do okre�lania, w jaki spos�b aplikacja reaguje na ��dania HTTP
        /// </summary>
        /// <param name="app"> Klasa dostarczaj�ca informacje na temat konfiguracji potoku ��da� </param>
        /// <param name="env"> Klasa przechowuj�ca informacje na temat hostowania aplikacji</param>
        /// <param name="databaseContext"> Klasa przechowuj�ca informacje na po��czenia z baz� danych</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext databaseContext)
        {
            databaseContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.ConfigureCustomExceptionMiddleware();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                });


            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
