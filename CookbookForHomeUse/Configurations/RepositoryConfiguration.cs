﻿namespace CookbookForHomeUse.Configurations
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories;
    using CookbookForHomeUse.Database.Repositories.Base;
    using Microsoft.Extensions.DependencyInjection;

    public static class RepositoryConfiguration
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Tag>, TagRepository>();
            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<IRepository<Fridge>, FridgeRepository>();
            services.AddScoped<IRepository<ProductInFridge>, ProductInFridgeRepository>();
            services.AddScoped<IRepository<Recipe>, RecipeRepository>();
            services.AddScoped<IRepository<RecipeCategory>, RecipeCategoryRepository>();
            services.AddScoped<IRepository<RecipeTag>, RecipeTagRepository>();
            services.AddScoped<IRepository<RecipeStep>, RecipeStepRepository>();
            services.AddScoped<IRepository<RecipeIngredient>, RecipeIngredientRepository>();
            return services;
        }
    }
}
