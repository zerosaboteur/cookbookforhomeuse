﻿namespace CookbookForHomeUse.Configurations
{
    using Microsoft.AspNetCore.Builder;

    /// <summary>
    /// Klasa wprowadzajaca dodaną metodę przetwarzania zapytań
    /// </summary>
    public static class ExceptionMiddlewareExtensions
    {
        /// <summary>
        /// Metoda dodaje do aplikacji własna metodę przetwarzania zapytań
        /// </summary>
        /// <param name="app"> Konfiguracja pipelinu aplikacji </param>
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
