﻿namespace CookbookForHomeUse.Configurations
{
    using CookbookForHomeUse.Services.Interfaces;
    using CookbookForHomeUse.Services.Services;

    using Microsoft.Extensions.DependencyInjection;

    public static class ServiceConfiguration
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<ITagService, TagService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IFridgeService, FridgeService>();
            services.AddScoped<IProductInFridgeService, ProductInFridgeService>();
            services.AddScoped<IRecipeService, RecipeService>();
            services.AddScoped<IRecipeCategoryService, RecipeCategoryService>();
            services.AddScoped<IRecipeTagService, RecipeTagService>();
            services.AddScoped<IRecipeStepService, RecipeStepService>();
            services.AddScoped<IRecipeIngredientService, RecipeIngredientService>();

            return services;
        }
    }
}
