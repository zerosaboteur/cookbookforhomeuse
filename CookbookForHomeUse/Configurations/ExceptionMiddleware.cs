﻿namespace CookbookForHomeUse.Configurations
{
    using System;
    using System.Net;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// Własna klasa przetwarzania błedów, jesli takowe wystąpiły w programie
    /// </summary>
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Konstruktor klasy
        /// </summary>
        /// <param name="next"></param>
        public ExceptionMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        /// <summary>
        /// Własny sposób wywołania zadania, tak aby możliwe było przechwycenie błędu
        /// </summary>
        /// <param name="httpContext"> Informacje odnośnie zapytania HTTP, które ma zostac wykonane </param>
        /// <returns> Status wykonania zadania </returns>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await this._next(httpContext);
            }
            catch (Exception ex)
            {
                await this.HandleExceptionAsync(httpContext, ex);
            }
        }

        /// <summary>
        /// Funkcja przetwarzająca wystąpienie błędu w zapytaniu
        /// </summary>
        /// <param name="context"> Informacje odnośnie zapytania HTTP, które ma zostac wykonane </param>
        /// <param name="exception"> Błąd działania </param>
        /// <returns> Status wykonania zadania </returns>
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            string response = "{\"statusCode\":" + context.Response.StatusCode + ",\"message\":\"" + exception.Message
                              + "\"}";

            return context.Response.WriteAsync(response);
        }
    }
}
