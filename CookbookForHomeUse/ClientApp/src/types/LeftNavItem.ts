import { Type } from '@angular/core';

export interface LeftNavItem {
  displayName: string;
  iconName: string;
  componentToDisplay?: Type<any>;
  expanded: boolean;
  children?: LeftNavItem[];
}
