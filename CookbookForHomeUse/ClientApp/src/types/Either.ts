export interface Either<TL, TR> {
  left: TL;
  right: TR;
  isLeft: boolean;
}

