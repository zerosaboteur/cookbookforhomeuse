export interface SuccessResponse {
  statusCode: number;
  message: string;
}
