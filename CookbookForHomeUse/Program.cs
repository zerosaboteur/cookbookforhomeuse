namespace CookbookForHomeUse
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;

    /// <summary>
    /// Klasa uruchamiana na samym pocz�tku uruchamiania aplikacji
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Metoda w kt�rej wywo�ujemt metod� inicjalizacji projektu
        /// </summary>
        /// <param name="args"> Zbi�r agrument�w </param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Inicjalizacja aplikacji
        /// </summary>
        /// <param name="args"> Zbi�r agrument�w </param>
        /// <returns> Konfiguracja hosta </returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();
                    });
    }
}
