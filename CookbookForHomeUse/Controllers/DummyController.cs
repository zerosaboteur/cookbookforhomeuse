﻿namespace CookbookForHomeUse.Controllers
{
    using System;

    using CookbookForHomeUse.Extensions.Either;
    using CookbookForHomeUse.ViewModels.ApiResponseViewModels;

    using Microsoft.AspNetCore.Mvc;

    public class Test
    {
        public int A { get; set; }

        public int B { get; set; }

        public int? C { get; set; }

        public int? D { get; set; }

        public string XD { get; set; }
    }

    /// <summary>
    /// Koontroler testowy, nie zawiera metod służacych czemukolwiek sensownemu, a jedyni pełni rolę sprawdzenia funkcjonalności
    /// </summary>
    [Route("api/user")]
    [ApiController]
    public class DummyController : Controller
    {
        /// <summary>
        /// Konstruktor kontrolera
        /// </summary>
        public DummyController()
        {
        }

        /// <summary>
        /// Metoda testowa
        /// </summary>
        /// <returns>Wynik działania metody</returns>
        [HttpGet("test")]
        public Test Test()
        {
            return new Test() { A = 1, C = null, XD = "ASDASDSDa" };
        }
    }
}
