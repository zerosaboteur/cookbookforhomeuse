﻿namespace CookbookForHomeUse.ViewModels.ProductViewModels
{
    using System;

    using CookbookForHomeUse.ViewModels.Enums;

    /// <summary>
    /// Budowa tabeli Product
    /// </summary>
    public class ProductViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nazwa produktu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Podstawowe jednostki miary produktu
        /// </summary>
        public MeasureUnits BaseMeasureUnit { get; set; }
    }
}
