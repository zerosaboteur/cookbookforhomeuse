﻿namespace CookbookForHomeUse.ViewModels.Enums
{
    /// <summary>
    /// Jednostki miary
    /// </summary>
    public enum MeasureUnits
    {
        /// <summary>
        /// Kilogramy
        /// </summary>
        Kg = 0,

        /// <summary>
        /// Dekagramy
        /// </summary>
        Dag = 1,

        /// <summary>
        /// Gramy
        /// </summary>
        G = 2,

        /// <summary>
        /// Litry
        /// </summary>
        L = 3,

        /// <summary>
        /// Mililitry
        /// </summary>
        Ml = 4,

        /// <summary>
        /// Łyżeczki
        /// </summary>
        Tbsp = 5

    }
}