﻿namespace CookbookForHomeUse.ViewModels.ProductInFridgeViewModels
{
    using System;

    using CookbookForHomeUse.ViewModels.Enums;

    /// <summary>
    /// Budowa tabeli ProductInFridge
    /// </summary>
    public class ProductInFridgeViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Data ważności produktu
        /// </summary>
        public DateTime ExpirationDateTime { get; set; }

        /// <summary>
        /// Ilość produktu
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// Jednostka miary
        /// </summary>
        public MeasureUnits MeasureUnit { get; set; }

        /// <summary>
        /// Klucz obcy tabeli Fridge
        /// </summary>
        public Guid FridgeId { get; set; }

        /// <summary>
        /// Klucz obcy tabeli Product 
        /// </summary>
        public Guid ProductId { get; set; }
    }
}
