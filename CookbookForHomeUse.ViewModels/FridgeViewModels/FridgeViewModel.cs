﻿namespace CookbookForHomeUse.ViewModels.FridgeViewModels
{
    using System;

    /// <summary>
    /// Budowa tabeli Fridge, do której przyłączone są dalej produkty w niej zawarte
    /// </summary>
    public class FridgeViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nazwa lodówki
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Opis lodówki
        /// </summary>
        public string Description { get; set; }
    }
}
