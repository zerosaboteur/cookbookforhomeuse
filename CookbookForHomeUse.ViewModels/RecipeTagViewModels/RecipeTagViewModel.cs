﻿namespace CookbookForHomeUse.ViewModels.RecipeTagViewModels
{
    using System;

    /// <summary>
    /// Budowa tabeli RecipeTag
    /// </summary>
    public class RecipeTagViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą Recipe
        /// </summary>
        public Guid RecpeId { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą Tag
        /// </summary>
        public Guid TagId { get; set; }
    }
}
