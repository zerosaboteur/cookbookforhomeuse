﻿namespace CookbookForHomeUse.ViewModels.ApiResponseViewModels
{
    using System.Text.Json;

    /// <summary>
    /// Odpowiedź błędu z API
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// Pusty konstruktor
        /// </summary>
        public ErrorResponse()
        {
        }

        /// <summary>
        /// Konstruktor obiektu
        /// </summary>
        /// <param name="code"></param>
        public ErrorResponse(int code)
        {
            this.StatusCode = code;
        }

        /// <summary>
        /// Konstruktor obiektu
        /// </summary>
        /// <param name="message"></param>
        public ErrorResponse(string message)
        {
            this.Message = message;
        }

        /// <summary>
        /// Konstruktor obiektu
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public ErrorResponse(int code, string message)
        {
            this.StatusCode = code;
            this.Message = message;
        }

        /// <summary>
        /// Kod błędu
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Wiadomość błędu do pokazania
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Metoda zamienia obiekt na string w postci JSON
        /// </summary>
        /// <returns> string w formacie JSON z wartościami obiektu </returns>
        public string ConvertObjectToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
