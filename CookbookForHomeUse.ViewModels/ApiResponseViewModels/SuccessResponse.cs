﻿namespace CookbookForHomeUse.ViewModels.ApiResponseViewModels
{
    using System.Text.Json;

    /// <summary>
    /// Wiadomość mówiąca o poprawnym wykonaniu zadania
    /// </summary>
    public class SuccessResponse
    {
        /// <summary>
        /// Pusty konstruktor obiektu
        /// </summary>
        public SuccessResponse()
        {
        }

        /// <summary>
        /// Konstruktor obiektu
        /// </summary>
        /// <param name="message"> Wiadomość dotycząca operacji </param>
        public SuccessResponse(string message)
        {
            this.StatusCode = 200;
            this.Message = message;
        }

        /// <summary>
        /// Konstruktor obiektu
        /// </summary>
        /// <param name="code"></param>
        public SuccessResponse(int code)
        {
            this.StatusCode = code;
        }

        /// <summary>
        /// Konstruktor obiektu
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public SuccessResponse(int code, string message)
        {
            this.StatusCode = code;
            this.Message = message;
        }

        /// <summary>
        /// Kod odpowiedzi, domyślnie nadawany jest 200
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Wiadomość dotycząca wykoanania operacji
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Metoda zamienia obiekt na string w postci JSON
        /// </summary>
        /// <returns> string w formacie JSON z wartościami obiektu </returns>
        public string ConvertObjectToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
