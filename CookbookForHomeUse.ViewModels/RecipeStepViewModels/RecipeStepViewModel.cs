﻿namespace CookbookForHomeUse.ViewModels.RecipeStepViewModels
{
    using System;

    /// <summary>
    /// Budowa tabeli ReciptStep zawierającej poszczególne kroki przepisu
    /// </summary>
    public class RecipeStepViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Numer kroku
        /// </summary>
        public int StepNumber { get; set; }

        /// <summary>
        /// Opis kroku
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą Recipe
        /// </summary>
        public Guid RecipeId { get; set; }
    }
}
