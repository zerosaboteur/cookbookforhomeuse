﻿namespace CookbookForHomeUse.ViewModels.RecipeViewModels
{
    using System;

    /// <summary>
    /// Budowa tabeli Recipe
    /// </summary>
    public class RecipeViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nazwa przepisu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Czas gotowania w minutach
        /// </summary>
        public decimal CookTime { get; set; }

        /// <summary>
        /// Ogólny opis przepisu
        /// </summary>
        public string Descriprion { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą RecipeCategory
        /// </summary>
        public Guid RecipeCategoryId { get; set; }
    }
}
