﻿namespace CookbookForHomeUse.ViewModels.RecipeCategoryViewModels
{
    using System;

    public class RecipeCategoryViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nazwa kategorii
        /// </summary>
        public string Name { get; set; }
    }
}
