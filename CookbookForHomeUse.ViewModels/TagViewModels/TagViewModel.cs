﻿namespace CookbookForHomeUse.ViewModels.TagViewModels
{
    using System;

    /// <summary>
    /// Budowa tabeli Tag
    /// </summary>
    public class TagViewModel
    {
        /// <summary>
        /// Identyfikator
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Tag
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Kolor tagu
        /// </summary>
        public string Color { get; set; }
    }
}
