﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class RecipeCategoryRepository : Repository<RecipeCategory>, IRepository<RecipeCategory>
    {
        public RecipeCategoryRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}