﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class RecipeRepository : Repository<Recipe>, IRepository<Recipe>
    {
        public RecipeRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}