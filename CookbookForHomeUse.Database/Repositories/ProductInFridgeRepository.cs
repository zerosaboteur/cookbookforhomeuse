﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class ProductInFridgeRepository : Repository<ProductInFridge>, IRepository<ProductInFridge>
    {
        public ProductInFridgeRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}