﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class RecipeIngredientRepository : Repository<RecipeIngredient>, IRepository<RecipeIngredient>
    {
        public RecipeIngredientRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}