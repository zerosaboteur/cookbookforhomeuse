﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class RecipeStepRepository : Repository<RecipeStep>, IRepository<RecipeStep>
    {
        public RecipeStepRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}