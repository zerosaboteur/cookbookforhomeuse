﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class FridgeRepository : Repository<Fridge>, IRepository<Fridge>
    {
        public FridgeRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}