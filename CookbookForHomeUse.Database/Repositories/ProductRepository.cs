﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class ProductRepository : Repository<Product>, IRepository<Product>
    {
        public ProductRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}