﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class RecipeTagRepository : Repository<RecipeTag>, IRepository<RecipeTag>
    {
        public RecipeTagRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}