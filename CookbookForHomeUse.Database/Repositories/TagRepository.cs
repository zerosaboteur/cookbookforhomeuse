﻿namespace CookbookForHomeUse.Database.Repositories
{
    using CookbookForHomeUse.Database.Models;
    using CookbookForHomeUse.Database.Repositories.Base;

    public class TagRepository : Repository<Tag>, IRepository<Tag>
    {
        public TagRepository(ApplicationDbContext _dbContext)
            : base(_dbContext)
        {
        }
    }
}
