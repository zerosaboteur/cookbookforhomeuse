﻿namespace CookbookForHomeUse.Database.Repositories.Base
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using CookbookForHomeUse.Database.Models;

    using Microsoft.EntityFrameworkCore;

    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        public Repository(ApplicationDbContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }

        /// <summary>
        /// Obiekt klasy bazy danych
        /// </summary>
        protected ApplicationDbContext RepositoryContext { get; set; }

        public IQueryable<T> Get()
        {
            return this.RepositoryContext.Set<T>();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> expression)
        {
            return this.RepositoryContext.Set<T>().Where(expression);
        }

        public async Task<bool> Any(Expression<Func<T, bool>> expression)
        {
            return await this.RepositoryContext.Set<T>().AnyAsync(expression);
        }

        public async Task<bool> Any(Guid id)
        {
            return await this.RepositoryContext.Set<T>().AnyAsync(n => n.Id.Equals(id));
        }

        public async Task<T> Get(Guid id)
        {
            return await this.RepositoryContext.Set<T>().FindAsync(id);
        }

        public virtual Task Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(entity)} entity must not be null");
            }

            return Task.FromResult(this.RepositoryContext.Set<T>().Add(entity));
        }

        public virtual Task Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(entity)} entity must not be null");
            }

            return Task.FromResult(this.RepositoryContext.Set<T>().Update(entity));
        }

        public virtual void Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
        }

        public virtual async Task Save()
        {
            await this.RepositoryContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            this.RepositoryContext.Dispose();
        }
    }
}
