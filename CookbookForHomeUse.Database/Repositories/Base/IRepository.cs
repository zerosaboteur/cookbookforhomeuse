﻿namespace CookbookForHomeUse.Database.Repositories.Base
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using CookbookForHomeUse.Database.Models;

    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Podstawowa metoda pobierajaca wszystkie dane z danej tabeli
        /// </summary>
        /// <returns> Obiekt z wartościami danej tabeli </returns>
        IQueryable<T> Get();

        /// <summary>
        /// Metoda pobierająca dane ze wskazanej tabeli, które spelniają podane warunki
        /// </summary>
        /// <param name="expression">Warunki do spełnienia</param>
        /// <returns>Obiekt z wartościami danej tabeli</returns>
        IQueryable<T> Get(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Metoda pobiera element z podanej tabeli ze wskzanym id
        /// </summary>
        /// <param name="id">Id suzkanego obiektu</param>
        /// <returns>Obiekt z wartościami danej tabeli</returns>
        Task<T> Get(Guid id);

        /// <summary>
        /// Metoda dodaje podany obiekt do wskazanej tabeli
        /// </summary>
        /// <param name="entity">Obiekt który odzwierciedla wygląd tabeli</param>
        /// <returns>Wynik dodania nowego wiersza</returns>
        Task Create(T entity);

        /// <summary>
        /// Metoda aktualizuje podany obiekt do wskazanej tabeli
        /// </summary>
        /// <param name="entity">Obiekt który odzwierciedla wygląd tabeli</param>
        /// <returns>Wynik aktualizacji wiersza</returns>
        Task Update(T entity);

        /// <summary>
        /// Metoda usuwa podany wiersz z tabeli
        /// </summary>
        /// <param name="entity">>Obiekt który odzwierciedla wygląd tabeli</param>
        void Delete(T entity);

        /// <summary>
        /// Metoda sprawdza czy isytnieje jakikolwiek wiersz pasujacy do podanego wyrażenia
        /// </summary>
        /// <param name="expression">Wyreżenie które musi spełnić jakikolwiek wiersz</param>
        /// <returns>Tak/Nie w zależności czy wiersz istnieje</returns>
        Task<bool> Any(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Metoda sprawdza czy istnieje jakolwiek wiersz posaidajacy wsakzane id
        /// </summary>
        /// <param name="id">Id do sprawdzenia czy istnieje wiersz</param>
        /// <returns>Tak/Nie w zależności czy znaleziono wiersz</returns>
        Task<bool> Any(Guid id);

        /// <summary>
        /// Garbage Collector
        /// </summary>
        void Dispose();

        /// <summary>
        /// Metoda zapisuje wprwoadzone zmiany
        /// </summary>
        /// <returns>Wynik zadania</returns>
        Task Save();
    }
}
