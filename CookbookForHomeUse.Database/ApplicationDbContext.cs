﻿namespace CookbookForHomeUse.Database
{
    using CookbookForHomeUse.Database.Models;

    using Microsoft.EntityFrameworkCore;

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Fridge> Fridges { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<ProductInFridge> ProductsInFridge { get; set; }

        public DbSet<Recipe> Recipes { get; set; }

        public DbSet<RecipeIngredient> RecipeIngredients { get; set; }

        public DbSet<RecipeStep> RecipeSteps { get; set; }

        public DbSet<RecipeTag> RecipeTags { get; set; }
    }
}