﻿namespace CookbookForHomeUse.Database.Models
{
    public class RecipeCategory : BaseEntity
    {
        /// <summary>
        /// Nazwa kategorii
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Połączenie z tabelą przepisu
        /// </summary>
        public virtual Recipe Recipe { get; set; }
    }
}
