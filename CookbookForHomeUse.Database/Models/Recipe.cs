﻿namespace CookbookForHomeUse.Database.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Budowa tabeli Recipe
    /// </summary>
    public class Recipe : BaseEntity
    {
        /// <summary>
        /// Nazwa przepisu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Czas gotowania w minutach
        /// </summary>
        public decimal CookTime { get; set; }

        /// <summary>
        /// Ogólny opis przepisu
        /// </summary>
        public string Descriprion { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą RecipeCategory
        /// </summary>
        public Guid RecipeCategoryId { get; set; }

        public virtual RecipeCategory RecipeCategory { get; set; }

        /// <summary>
        /// Połączenie z tabelą RecipeTag
        /// </summary>
        public ICollection<RecipeTag> RecipeTags { get; set; }

        /// <summary>
        /// Połączenie z tabelą RecipeTag
        /// </summary>
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }

        /// <summary>
        /// Połączenie z tabelą RecipeStep
        /// </summary>
        public ICollection<RecipeStep> RecipeSteps { get; set; }
    }
}
