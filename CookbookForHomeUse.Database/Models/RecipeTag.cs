﻿namespace CookbookForHomeUse.Database.Models
{
    using System;

    /// <summary>
    /// Budowa tabeli RecipeTag
    /// </summary>
    public class RecipeTag : BaseEntity
    {
        /// <summary>
        /// Klucz obcy i połączenie z tabelą Recipe
        /// </summary>
        public Guid RecpeId { get; set; }

        public Recipe Recipe { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą Tag
        /// </summary>
        public Guid TagId { get; set; }

        public Tag Tag { get; set; }
    }
}
