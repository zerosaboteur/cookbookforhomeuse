﻿namespace CookbookForHomeUse.Database.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Budowa tabeli Fridge, do której przyłączone są dalej produkty w niej zawarte
    /// </summary>
    public class Fridge : BaseEntity
    {
        /// <summary>
        /// Nazwa lodówki
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Opis lodówki
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Utworzenie relacji One-to-Many z tabelą Fridge
        /// </summary>
        public ICollection<ProductInFridge> ProductsInFridge { get; set; }
    }
}
