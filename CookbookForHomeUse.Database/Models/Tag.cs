﻿namespace CookbookForHomeUse.Database.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Budowa tabeli Tag
    /// </summary>
    public class Tag : BaseEntity
    {
        /// <summary>
        /// Tag
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Kolor tagu
        /// </summary>
        public string Color { get; set; }

        public ICollection<RecipeTag> RecipeTag { get; set; }
    }
}
