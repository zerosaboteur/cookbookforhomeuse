﻿namespace CookbookForHomeUse.Database.Models
{
    using System;

    using CookbookForHomeUse.ViewModels.Enums;

    /// <summary>
    /// Budowa tabeli RecipeIngredient
    /// </summary>
    public class RecipeIngredient : BaseEntity
    {
        /// <summary>
        /// Ilość danego składniku
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// Rodzaj jednostki miary
        /// </summary>
        public MeasureUnits MeasureUnit { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą Recipe
        /// </summary>
        public Guid RecipeId { get; set; }

        public Recipe Recipe { get; set; }

        /// <summary>
        /// Klucz obcy i połączenie z tabelą Product
        /// </summary>
        public Guid ProductId { get; set; }
        
        public Product Product { get; set; }
    }
}
