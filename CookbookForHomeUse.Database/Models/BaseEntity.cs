﻿namespace CookbookForHomeUse.Database.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Klasa zawirajaca klucz główny dla wszystkich tabel
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Klucz główny
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
    }
}
