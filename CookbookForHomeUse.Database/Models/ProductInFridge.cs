﻿namespace CookbookForHomeUse.Database.Models
{
    using System;

    using CookbookForHomeUse.ViewModels.Enums;

    /// <summary>
    /// Budowa tabeli ProductInFridge
    /// </summary>
    public class ProductInFridge : BaseEntity
    {
        /// <summary>
        /// Data ważności produktu
        /// </summary>
        public DateTime ExpirationDateTime { get; set; }

        /// <summary>
        /// Ilość produktu
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// Jednostka miary
        /// </summary>
        public MeasureUnits MeasureUnit { get; set; }

        /// <summary>
        /// Klucz obcy tabeli Fridge i połączenie One-to-Many
        /// </summary>
        public Guid FridgeId { get; set; }

        public Fridge Fridge { get; set; }

        /// <summary>
        /// Klucz obcy tabeli Product i połączenie One-to-Many
        /// </summary>
        public Guid ProductId { get; set; }

        public Product Product { get; set; }
    }
}
