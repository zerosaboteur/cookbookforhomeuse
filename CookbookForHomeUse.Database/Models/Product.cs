﻿namespace CookbookForHomeUse.Database.Models
{
    using System.Collections.Generic;

    using CookbookForHomeUse.ViewModels.Enums;

    /// <summary>
    /// Budowa tabeli Product
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// Nazwa produktu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Podstawowe jednostki miary produktu
        /// </summary>
        public MeasureUnits BaseMeasureUnit { get; set; }

        /// <summary>
        /// Połączenie z tabelą ProductInFridge
        /// </summary>
        public ICollection<ProductInFridge> ProductsInFridge { get; set; }

        /// <summary>
        /// Połączenie z tabelą RecipeIngredients
        /// </summary>
        public ICollection<RecipeIngredient> RecipeIngredients { get; set; }
    }
}
